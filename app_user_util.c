
/*******************************
 *	INCLUDES
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "boards.h"

#include "nordic_common.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "bsp.h"

#include "app_uart.h"
#include "app_error.h"
#include "app_util.h"

#include "app_user_util.h"


/*******************************
 *	CONSTANTS
 */
 
 
 /*******************************
 *	MACROS
 */


uint32_t ticks_to_us_(uint32_t ticks, uint8_t prescaler, uint32_t frequency)
{	
	float numerator = ((float)prescaler + 1.0f) * 1000000.0f;
  float denominator = (float)frequency;
  float ms_per_tick = numerator / denominator;

  uint32_t us = ms_per_tick * ticks;

  return us;
}

uint32_t ticks_to_ms_(uint32_t ticks, uint8_t prescaler, uint32_t frequency)
{
	float numerator = ((float)prescaler + 1.0f) * 1000.0f;
  float denominator = (float)frequency;
  float ms_per_tick = numerator / denominator;

  uint32_t ms = ms_per_tick * ticks;

  return ms;
}

uint32_t ticks_to_us(uint32_t ticks, uint8_t prescaler, uint32_t frequency)
{
	//us = tick * (prescalar +1) * 1000000/ frequency
	return (uint32_t)ROUNDED_DIV((ticks*((float)prescaler + 1.0f)*1000000.0f), (float)frequency);
}

uint16_t ticks_to_ms(uint32_t ticks, uint8_t prescaler, uint32_t frequency)
{
	//ms = tick * (prescalar +1) * 1000/ frequency
	return (uint16_t)ROUNDED_DIV((ticks*((float)prescaler + 1.0f)*1000.0f), (float)frequency);
}

/** @} */
