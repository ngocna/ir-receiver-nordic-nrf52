
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef APP_USER_UTIL_H__
#define APP_USER_UTIL_H__

/*******************************
 *	INCLUDES
 */
#include "nordic_common.h"
#include "sdk_config.h"
#include "sdk_errors.h"
#include "nrf_assert.h"

#ifdef __cplusplus
extern "C" {
#endif
	
	
 /*******************************
 *	MACROS
 */
#define LED_TURN_ON(ledPin)				nrf_drv_gpiote_out_clear(ledPin);
#define LED_TURN_OFF(ledPin)			nrf_drv_gpiote_out_set(ledPin);	
	
	
/**@brief .
 *
 * @param[in]  ticks    
 * @param[in]  prescaler
 * @param[in]  frequency 
 *
 * @retval                  .
 */
uint32_t ticks_to_us_(uint32_t ticks, uint8_t prescaler, uint32_t frequency);

/**@brief .
 *
 * @param[in]  ticks    
 * @param[in]  prescaler
 * @param[in]  frequency 
 *
 * @retval                  .
 */
uint32_t ticks_to_ms_(uint32_t ticks, uint8_t prescaler, uint32_t frequency);
	
/**@brief .
 *
 * @param[in]  ticks    
 * @param[in]  prescaler
 * @param[in]  frequency 
 *
 * @retval                  .
 */
uint32_t ticks_to_us(uint32_t ticks, uint8_t prescaler, uint32_t frequency);

/**@brief .
 *
 * @param[in]  ticks    
 * @param[in]  prescaler
 * @param[in]  frequency 
 *
 * @retval                  .
 */
uint16_t ticks_to_ms(uint32_t ticks, uint8_t prescaler, uint32_t frequency);




#ifdef __cplusplus
}
#endif

#endif /* APP_USER_UTIL_H__ */

/** @} */
