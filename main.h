
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "core_cm4.h"                               /*!< Cortex-M4 processor and core peripherals                              */
#include "system_nrf52.h"                           /*!< nrf52 System                                                          */


#ifndef NO
#define NO	0
#endif

typedef bool StatusYesOrNo_t;

//------------------------------------------------------------------------------
// Debug directives
//
#if DEBUG
#	define DBG_PRINT(...)    printf(__VA_ARGS__)
#	define DBG_PRINTLN(...)  printf(__VA_ARGS__);	printf("\r\n")
#else
#	define DBG_PRINT(...)
#	define DBG_PRINTLN(...)
#endif

