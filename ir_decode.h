
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __IR_DECODE_H
#define __IR_DECODE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "main.h"
#include "core_cm4.h"                               /*!< Cortex-M4 processor and core peripherals                              */

/** @addtogroup IR_REMOTE
  * @brief Infra-red remote control
  * @{
  */

/** @addtogroup SIRC_DECODE
  * @brief SIRC decode driver modules
  * @{
  */

/** @defgroup SIRC_Exported_Types
  * @{
  */

/**
  * @brief  SIRC frame structure
  */
typedef struct
{
  __IO uint8_t Command;   /*!< Command field */
  __IO uint8_t Address;   /*!< Address field */
}
SIRC_Frame_t;

/**
  * @brief  SIRC packet structure
  */
typedef struct
{
  uint8_t count;  /*!< Bit count */
  uint8_t status; /*!< Status */
  uint32_t data;  /*!< Data */
}
SIRC_packet_t;

/**
  * @}
  */

/** @defgroup SIRC_Exported_Functions
  * @{
  */
void Menu_SIRCDecode_Func(void);
void SIRC_ResetPacket(void);
void SIRC_DataSampling(uint32_t lowPulseLength, uint32_t wholePulseLength);

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* __IR_DECODE_H */
