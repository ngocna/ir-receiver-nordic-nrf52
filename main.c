
/** @file
 * @defgroup uart_example_main main.c
 * @{
 * @ingroup uart_example
 * @brief UART Example Application main file.
 *
 * This file contains the source code for a sample application using UART.
 *
 */

/*******************************
 *	INCLUDES
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "main.h"

#include "boards.h"

#include "nordic_common.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "bsp.h"

#include "app_uart.h"
#include "app_error.h"
#include "app_util.h"
#include "app_user_util.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "nrf_drv_gpiote.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"


/*******************************
 *	CONSTANTS
 */
//GPIO constants
#ifdef BSP_BUTTON_0
    #define PIN_IN BSP_BUTTON_0
#endif
#ifndef PIN_IN
    #error "Please indicate input pin"
#endif

#ifdef BSP_LED_0
    #define PIN_OUT BSP_LED_0
#endif
#ifndef PIN_OUT
    #error "Please indicate output pin"
#endif

#define	BOARD_LED_1	0
#define	BOARD_LED_2	1
#define	BOARD_LED_3	2
#define	BOARD_LED_4	3

//UART constants
#define MAX_TEST_DATA_BYTES     (15U)                	/**< max number of test bytes to be used for tx and rx. */
#define UART_TX_BUF_SIZE 	256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 	256                         /**< UART RX buffer size. */
/* When UART is used for communication with the host do not use flow control.*/
#define UART_HWFC 				APP_UART_FLOW_CONTROL_DISABLED

//PPI constants
#define PPI_EXAMPLE_TIMER0_INTERVAL             (100)   // Timer interval in milliseconds

//App define
//#define FREQUENCY_COUNTER

//#define IR_POLARITY_TEST
#ifdef IR_POLARITY_TEST
#define IR_PIN_IN						BSP_BUTTON_0
#define IR_TEST_PIN_OUT			LED_2
#define IR_INPIN_PULL				NRF_GPIO_PIN_PULLUP
#define IR_POLARITY_START		NRF_GPIOTE_POLARITY_HITOLO
#define IR_POLARITY_FALLING	NRF_GPIOTE_POLARITY_LOTOHI
#define IR_HIGH							0
#else
#define IR_PIN_IN						BSP_BUTTON_0
#define IR_TEST_PIN_OUT			LED_2
#define IR_INPIN_PULL				NRF_GPIO_PIN_PULLDOWN
#define IR_POLARITY_START		NRF_GPIOTE_POLARITY_LOTOHI
#define IR_POLARITY_FALLING	NRF_GPIOTE_POLARITY_HITOLO
#define IR_HIGH							1

#endif


/*******************************
 *	MACROS
 */
 

/*******************************
 *	LOCAL VARIABLES
 */
static const nrf_drv_timer_t m_timer0 = NRF_DRV_TIMER_INSTANCE(0);
//static const nrf_drv_timer_t m_timer1 = NRF_DRV_TIMER_INSTANCE(1);
//static const nrf_drv_timer_t m_timer2 = NRF_DRV_TIMER_INSTANCE(2);

static nrf_ppi_channel_t m_ppi_channel0;
//static nrf_ppi_channel_t m_ppi_channel2;


volatile static uint32_t counter0 = 0;
/*volatile static uint32_t counter1 = 0;
volatile static uint32_t counter2 = 0;
volatile static uint32_t counter3 = 0;*/

/*
    NRF_TIMER_FREQ_16MHz = 0, ///< Timer frequency 16 MHz.
    NRF_TIMER_FREQ_8MHz,      ///< Timer frequency 8 MHz.
    NRF_TIMER_FREQ_4MHz,      ///< Timer frequency 4 MHz.
    NRF_TIMER_FREQ_2MHz,      ///< Timer frequency 2 MHz.
    NRF_TIMER_FREQ_1MHz,      ///< Timer frequency 1 MHz.
    NRF_TIMER_FREQ_500kHz,    ///< Timer frequency 500 kHz.
    NRF_TIMER_FREQ_250kHz,    ///< Timer frequency 250 kHz.
    NRF_TIMER_FREQ_125kHz,    ///< Timer frequency 125 kHz.
    NRF_TIMER_FREQ_62500Hz,   ///< Timer frequency 62500 Hz.
    NRF_TIMER_FREQ_31250Hz    ///< Timer frequency 31250 Hz.
*/
const uint32_t aFrequency[] = 
							{ 16000000, 8000000, 4000000, 2000000, 1000000, 500000, 250000, 125000, 62500, 31250 };


/*******************************
 *	FUNCTIONS DECLARE
 */

void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_init(void)
{
    uint32_t                     err_code;
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
        .baud_rate    = NRF_UART_BAUDRATE_115200
    };
		
    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_error_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
		
		printf("\r\n[FC]uart_init\r\n");
}

void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
		if(pin == IR_PIN_IN)
		{
			if(action == IR_POLARITY_START)
			{
				
			}
			else if(action == IR_POLARITY_FALLING)
			{

			}
			else
			{
				if(nrf_gpio_pin_read(IR_PIN_IN) == IR_HIGH)
				{
					LED_TURN_ON(PIN_OUT);
					counter0 = nrf_drv_timer_capture(&m_timer0,NRF_TIMER_CC_CHANNEL0);
					//printf("\r\ncounter0: %d\r\n", counter0);
				}
				else
				{
					LED_TURN_OFF(PIN_OUT);

					counter0 = nrf_drv_timer_capture(&m_timer0,NRF_TIMER_CC_CHANNEL0) - counter0;

					//printf("\r\ncounter1: %d\r\n", counter0);
					printf("us : %d us.\r\n", ticks_to_us_(counter0, 0, aFrequency[TIMER_DEFAULT_CONFIG_FREQUENCY]));
					//printf("ms : %d\r\n", ticks_to_ms(counter0, 0, aFrequency[TIMER_DEFAULT_CONFIG_FREQUENCY]));
					counter0 = 0;
				}
				
			}
		}
}

/**
 * @brief Function for configuring: IR_PIN_IN pin for input, PIN_OUT pin for output,
 * and configures GPIOTE to give an interrupt on pin change.
 */
static void gpio_init(void)
{
		printf("[FC]gpio_init\r\n");
	
	{
		ret_code_t err_code;

		//========== SET INPUT GPIOTE ==========
		//Init GPIOTE driver in general
    err_code = nrf_drv_gpiote_init();
				APP_ERROR_CHECK(err_code);

		//Default in_config parameters
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    in_config.pull = NRF_GPIO_PIN_PULLUP;
		//in_config.sense = NRF_GPIOTE_POLARITY_HITOLO;

		//Init GPIO driver for IR_PIN_IN
    err_code = nrf_drv_gpiote_in_init(IR_PIN_IN, &in_config, in_pin_handler);	//in_pin_handler
				APP_ERROR_CHECK(err_code);
	
		//Enable interrupt on IR_PIN_IN
    nrf_drv_gpiote_in_event_enable(IR_PIN_IN, true);
		
		
		//========== SET OUT GPIOTE ==========
    nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_TASK_TOGGLE(true);
		nrf_drv_gpiote_out_init(IR_TEST_PIN_OUT, &out_config);
		
		nrf_drv_gpiote_out_task_enable(IR_TEST_PIN_OUT);
	}
}


/** TIMTER0 peripheral interrupt handler. This interrupt handler is called whenever there it a TIMER0 interrupt
 */
/*void TIMER0_IRQHandler(void)
{
	printf("[FC]TIMER0_IRQHandler\r\n");
  if ((NRF_TIMER0->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER0->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
  {
		NRF_TIMER0->EVENTS_COMPARE[0] = 0;	//Clear compare register 0 event	
		nrf_gpio_pin_set(PIN_OUT);         	//Set LED
		//bsp_board_led_on(BOARD_LED_1);
  }
	
	if ((NRF_TIMER0->EVENTS_COMPARE[1] != 0) && ((NRF_TIMER0->INTENSET & TIMER_INTENSET_COMPARE1_Msk) != 0))
  {
		NRF_TIMER0->EVENTS_COMPARE[1] = 0;	//Clear compare register 1 event
		nrf_gpio_pin_clear(PIN_OUT);       	//Clear LED
		//bsp_board_led_off(BOARD_LED_1);
  }
}*/

/** @brief Function for initializing Programmable Peripheral Interconnect (PPI) peripheral.
 *   The PPI is needed to convert the timer event into a task.
 */
static void ppi_init(void)
{

	  uint32_t err_code = NRF_SUCCESS;

    err_code = nrf_drv_ppi_init();
			APP_ERROR_CHECK(err_code);
	
	  /* Configure 1st available PPI channel to start TIMER0 counter at ... match,
     * which is every odd number of seconds.
     */
    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel0);
			APP_ERROR_CHECK(err_code);
	
		/*
			NRF_TIMER_TASK_START
			NRF_TIMER_TASK_STOP
			NRF_TIMER_TASK_COUNT
			NRF_TIMER_TASK_CLEAR
			NRF_TIMER_TASK_SHUTDOWN
			NRF_TIMER_TASK_CAPTUREx
		*/
		//Task endpoint assignment	
    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel0,
                                          nrf_drv_gpiote_in_event_addr_get(IR_PIN_IN),
                                          nrf_drv_timer_task_address_get(&m_timer0,
                                                                         NRF_TIMER_TASK_START));
			APP_ERROR_CHECK(err_code);
			
		//Fork assignment	
		err_code = nrf_drv_ppi_channel_fork_assign(m_ppi_channel0, 
																							 nrf_drv_gpiote_out_task_addr_get(IR_TEST_PIN_OUT));
			APP_ERROR_CHECK(err_code);
	
	  // Enable both configured PPI channels
    err_code = nrf_drv_ppi_channel_enable(m_ppi_channel0);
			APP_ERROR_CHECK(err_code);

    //============================================================================================
		// Configure PPI channel 0 to toggle GPIO_OUTPUT_PIN on every TIMER0 COMPARE[0] match (200 ms)
   /*NRF_PPI->CH[0].EEP = (uint32_t)&NRF_GPIOTE->EVENTS_IN[0];
		NRF_PPI->CH[0].TEP = (uint32_t)&NRF_TIMER0->TASKS_CAPTURE[0];

    // Enable PPI channel 0
    NRF_PPI->CHEN = (PPI_CHEN_CH0_Enabled << PPI_CHEN_CH0_Pos);
	*/
}

static void timer0_event_handler(nrf_timer_event_t event_type, void * p_context)
{
	printf("[FC]timer0_event_handler\r\n");
  if ((NRF_TIMER0->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER0->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
  {
		NRF_TIMER0->EVENTS_COMPARE[0] = 0;	//Clear compare register 0 event	
		nrf_gpio_pin_set(PIN_OUT);         	//Set LED
  }
	
	if ((NRF_TIMER0->EVENTS_COMPARE[1] != 0) && ((NRF_TIMER0->INTENSET & TIMER_INTENSET_COMPARE1_Msk) != 0))
  {
		NRF_TIMER0->EVENTS_COMPARE[1] = 0;	//Clear compare register 1 event
		nrf_gpio_pin_clear(PIN_OUT);       	//Clear LED
  }
}

/**
 * @brief Function for configuring: ,
 * .
 */
void start_timer(void)
{
  NRF_TIMER0->MODE = TIMER_MODE_MODE_Counter;              // Set the timer in Counter Mode
  NRF_TIMER0->TASKS_CLEAR = 1;                             // clear the task first to be usable for later
	NRF_TIMER0->BITMODE = TIMER_BITMODE_BITMODE_32Bit;		   //Set counter to 32 bit resolution
	NRF_TIMER0->CC[0] = 500000;                              //Set value for TIMER0 compare register 0
	NRF_TIMER0->CC[1] = 400000;                              //Set value for TIMER0 compare register 1
		
  // Enable interrupt on Timer 0, both for CC[0] and CC[1] compare match events
	NRF_TIMER0->SHORTS |= (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);
	NRF_TIMER0->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos) | (TIMER_INTENSET_COMPARE1_Enabled << TIMER_INTENSET_COMPARE1_Pos);
  NVIC_EnableIRQ(TIMER0_IRQn);
		
  NRF_TIMER0->TASKS_START = 1;               // Start TIMER0
}

/** @brief Function for Timer 0 initialization.
 *  @details 
 *           
 */
static void timer0_init(void)
{
		printf("[FC]timer0_init\r\n");
	
	  // Check TIMER0 configuration for details.
    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
		timer_cfg.mode = NRF_TIMER_MODE_TIMER;
	
	  ret_code_t err_code = nrf_drv_timer_init(&m_timer0, &timer_cfg, timer0_event_handler);
    APP_ERROR_CHECK(err_code);
				
		// Start clock.
		nrf_drv_timer_enable(&m_timer0);
}


/**
 * @brief Function for main application entry.
 */
int main(void)
{
    
		uart_init();

		bsp_board_leds_init();

		gpio_init();
	
		ppi_init();

		//start_timer();
		timer0_init();
	
    while (true)
    {

    }
}


/** @} */
